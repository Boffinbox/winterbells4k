﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler Instance;
    private Transform objectPoolTransform;
    private void Awake()
    {
        Instance = this;
        objectPoolTransform = this.transform;
    }
    [SerializeField] GameObject bellObject;
    [SerializeField] GameObject boostBellObject;
    public List<GameObject> PoolOfBells { get; set; } = new List<GameObject>();
    public List<GameObject> PoolOfBoostBells { get; set; } = new List<GameObject>();

    private List<List<GameObject>> listOfPools = new List<List<GameObject>>();

    private void Start()
    {
        listOfPools.Add(PoolOfBells);
        listOfPools.Add(PoolOfBoostBells);
    }
    /// <summary>
    /// Fetches an object from the requested pool, and sets it active.
    /// </summary>
    /// <param name="poolList">A list to pull from. You should only feed in lists from ObjectPooler.</param>
    public GameObject GetObjectFromPool(List<GameObject> poolList)
    {
        if (poolList.Count <= 0)
        {
            ExpandObjectPools();
        }
        for (int i = 0; i < poolList.Count; i++)
        {
            if (!poolList[i].activeInHierarchy)
            {
                GameObject requestedObject = poolList[i];
                poolList.Remove(requestedObject);
                ToggleObjectActive(requestedObject, true);
                return requestedObject;
            }
        }
        return null;
    }
    private void ExpandObjectPools()
    {
        foreach (List<GameObject> a in listOfPools)
        {
            if (a.Count <= 0)
            {
                ExpandThisPool(a);
            }
        }
    }
    private void ExpandThisPool(List<GameObject> a)
    {
        if (a == PoolOfBells)
        {
            ExpandPool(a, bellObject);
        }
        if (a == PoolOfBoostBells)
        {
            ExpandPool(a, boostBellObject);
        }
    }
    private void ExpandPool(List<GameObject> poolList, GameObject a)
    {
        GameObject newObject = Instantiate(a, objectPoolTransform);
        newObject.SetActive(false);
        poolList.Add(newObject);
    }
    private void ResetObjectState(GameObject objectToReturn)
    {
        ToggleObjectActive(objectToReturn, false);
        objectToReturn.transform.SetParent(objectPoolTransform);
        objectToReturn.transform.localPosition = Vector3.zero;
        objectToReturn.transform.localEulerAngles = Vector3.zero;
        objectToReturn.transform.localScale = new Vector3(1, 1, 1);
        foreach (Transform child in objectToReturn.transform)
        {
            child.transform.localPosition = new Vector3(0, 0, 0);
            if (child.GetComponent<SpriteRenderer>())
            {
                child.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            }
            if (child.GetComponent<TextMeshProUGUI>())
            {
                child.GetComponent<TextMeshProUGUI>().color = new Color(1, 1, 1, 0);
            }
        }
        if (objectToReturn.GetComponent<Rigidbody>())
        {
            Rigidbody rb = objectToReturn.GetComponent<Rigidbody>();
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            rb.isKinematic = true;
            rb.MoveRotation(Quaternion.identity);
        }
    }
    private void ToggleObjectActive(GameObject o, bool enabled)
    {
        o.SetActive(enabled);
    }
    /// <summary>
    /// Use this method to pass the collected item back to the object pooler. You should only pass "normal" collectables.
    /// </summary>
    /// <param name="objectToReturn">The GameObject you wish to return.</param>
    public void ReturnObjectToBellsPool(GameObject objectToReturn)
    {
        ReturnObjectRoutine(objectToReturn, PoolOfBells);
    }
    /// <summary>
    /// Use this method to pass the collected item back to the object pooler. You should only pass "organizer" GameObjects
    /// </summary>
    /// <param name="objectToReturn">The GameObject you wish to return.</param>
    public void ReturnObjectToBoostBellsPool(GameObject objectToReturn)
    {
        ReturnObjectRoutine(objectToReturn, PoolOfBoostBells);
    }

    private void ReturnObjectRoutine(GameObject objectToReturn, List<GameObject> returnList)
    {
        ResetObjectState(objectToReturn);
        returnList.Add(objectToReturn);
    }
}