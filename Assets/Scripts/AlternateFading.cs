﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlternateFading : MonoBehaviour
{
    [SerializeField] GameObject newHighScore;
    [SerializeField] GameObject excellent;
    Graphic newHighScoreGraphic;
    Graphic excellentGraphic;
    enum pongState { Excellent, NewHigh }
    pongState state;
    private void OnEnable()
    {
        excellent.SetActive(true);
        newHighScore.SetActive(true);
        excellentGraphic = excellent.GetComponent<Graphic>();
        newHighScoreGraphic = newHighScore.GetComponent<Graphic>();
        excellentGraphic.color = new Color(1, 1, 1, 1);
        StartCoroutine(AlternateFade());
    }
    private void Update()
    {
    }
    IEnumerator AlternateFade()
    {
        yield return new WaitForSeconds(0.5f);
        while (true)
        {
            if (excellentGraphic.color.a <= 0 && newHighScoreGraphic.color.a >= 1 && state == pongState.NewHigh)
            {
                yield return new WaitForSeconds(0.5f);
                StartCoroutine(FadeSequence(excellentGraphic, 0.5f, 0.5f, 1));
                StartCoroutine(FadeSequence(newHighScoreGraphic, 0.5f, 0, 0));
                state = pongState.Excellent;
                yield return null;
            }
            else if (newHighScoreGraphic.color.a <= 0 && excellentGraphic.color.a >= 1 && state == pongState.Excellent)
            {
                yield return new WaitForSeconds(0.5f);
                StartCoroutine(FadeSequence(newHighScoreGraphic, 0.5f, 0.5f, 1));
                StartCoroutine(FadeSequence(excellentGraphic, 0.5f, 0, 0));
                state = pongState.NewHigh;
                yield return null;
            }
            yield return null;
        }
    }
    IEnumerator FadeSequence(Graphic elementToFade, float fadeTime, float delayTime, int fadeType)
    {
        elementToFade.color = new Color(elementToFade.color.r, elementToFade.color.g, elementToFade.color.b, (1 - fadeType));
        yield return new WaitForSeconds(delayTime);
        float reciprocal = 1 / fadeTime;
        if (fadeType == 1)
        {
            reciprocal *= 1;
        }
        else if (fadeType == 0)
        {
            reciprocal *= -1;
        }
        for (float f = 0f; f <= fadeTime * 1.05f; f += Time.deltaTime)
        {
            elementToFade.color = new Color(elementToFade.color.r, elementToFade.color.g, elementToFade.color.b, (1 - fadeType) + (f * reciprocal));
            yield return null;
        }
    }
}
