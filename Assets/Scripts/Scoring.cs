﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Scoring : MonoBehaviour
{
    static int bellCount = 0;
    public static int BellCount { get => bellCount; }
    ulong score = 0;
    ulong exponentScore;
    bool exponentMode;
    [SerializeField] GameObject doubleImage;
    [SerializeField] TextMeshProUGUI exponentText;
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] CanvasGroup firstScoreWindow;
    [SerializeField] TextMeshProUGUI firstScoreWindowScoreText;
    [SerializeField] CanvasGroup scoreWindow;
    [SerializeField] TextMeshProUGUI scoreWindowScoreText;
    [SerializeField] TextMeshProUGUI scoreWindowPreviousScoreText;
    [SerializeField] GameObject scoreWindowYourScore;
    [SerializeField] GameObject scoreWindowExcellentScore;
    private void Awake()
    {
        bellCount = 0;
    }
    private void Update()
    {
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<BoostBell>())
        {
            BoostBellScoring(other);
        }
        else
        {
            BellScoring(other);
        }
        if (CountDigits(score) <= 16)
        {
            scoreText.text = score.ToString();
        }
        else
        {
            exponentMode = true;
            score /= 10;
            exponentScore++;
            scoreText.text = score.ToString();
        }
        if (exponentScore > 0)
        {
            exponentText.text = exponentScore.ToString();
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (bellCount > 0)
        {
            StartCoroutine(FadeSequence(scoreText, 0.3f, 0, 0));
            if (exponentScore > 0)
            {
                StartCoroutine(FadeSequence(exponentText, 0.3f, 0, 0));
            }
            if (int.Parse(GetHighScore()[0].ToString()) > 0)
            {
                EnableScoreWindow(scoreWindow, scoreWindowScoreText);
                scoreWindowPreviousScoreText.text = GetHighScore();
            }
            else
            {
                EnableScoreWindow(firstScoreWindow, firstScoreWindowScoreText);
            }
            string highScoreString = GetHighScore();
            ulong highScore;
            ulong highExponent;
            if (highScoreString.Length <= 16)
            {
                highScore = ulong.Parse(highScoreString);
                if (exponentScore > 0)
                {
                    SetHighScore(score, exponentScore);
                }
                else if (score > highScore)
                {
                    SetHighScore(score);
                }
            }
            else
            {
                highScore = ulong.Parse(highScoreString.Substring(0, 16));
                highExponent = ulong.Parse(highScoreString.Substring(16));
                if (exponentScore > highExponent)
                {
                    SetHighScore(score, exponentScore);
                }
                else if ((score > highScore) && (exponentScore == highExponent))
                {
                    SetHighScore(score, exponentScore);
                }
            }
        }
    }
    void EnableScoreWindow(CanvasGroup scoreWindow, TextMeshProUGUI scoreWindowText)
    {
        scoreWindow.gameObject.SetActive(true);
        SetFinalScoreText(scoreWindowText);
        StartCoroutine(FadeSequence(scoreWindow, 0.3f, 0, 1));
    }
    void SetFinalScoreText(TextMeshProUGUI scoreText)
    {
        if (exponentScore > 0)
        {
            scoreText.text = score.ToString() + exponentScore.ToString();
        }
        else
        {
            scoreText.text = score.ToString();
        }
    }
    void BoostBellScoring(Collider2D other)
    {
        score *= 2;
        Vector2 screenPos = Camera.main.WorldToScreenPoint(other.transform.position);
        RectTransformUtility.PixelAdjustPoint(screenPos, doubleImage.transform, doubleImage.GetComponentInParent<Canvas>());
        doubleImage.transform.position = screenPos;
        StartCoroutine(FadeSequence(doubleImage.GetComponent<Image>(), 0.1f, 0.4f, 0));
    }
    void BellScoring(Collider2D other)
    {
        bellCount++;
        ulong scoreToAdd = (ulong)BellCount * 10;
        if (exponentScore <= 10 && exponentMode)
        {
            scoreToAdd /= (10 ^ exponentScore);
        }
        else if (exponentScore > 10)
        {
            // do nothing, the player is in full exponent tryhard mode now
        }
        else
        {
            score += scoreToAdd;
        }
        other.GetComponentInChildren<TextMeshProUGUI>().SetText((BellCount * 10).ToString());
    }
    string GetHighScore()
    {
        return PlayerPrefs.GetString("highScore", "0");
    }
    void SetHighScore(ulong score)
    {
        PlayerPrefs.SetString("highScore", score.ToString());
        scoreWindowYourScore.SetActive(false);
        scoreWindowExcellentScore.SetActive(true);
    }
    void SetHighScore(ulong score, ulong exponentScore)
    {
        PlayerPrefs.SetString("highScore", score.ToString() + exponentScore.ToString());
        scoreWindowYourScore.SetActive(false);
        scoreWindowExcellentScore.SetActive(true);
    }
    IEnumerator FadeSequence(Graphic elementToFade, float fadeTime, float delayTime, int fadeType)
    {
        elementToFade.color = new Color(elementToFade.color.r, elementToFade.color.g, elementToFade.color.b, (1 - fadeType));
        yield return new WaitForSeconds(delayTime);
        float reciprocal = 1 / fadeTime;
        if (fadeType == 1)
        {
            reciprocal *= 1;
        }
        else if (fadeType == 0)
        {
            reciprocal *= -1;
        }
        for (float f = 0f; f <= fadeTime * 1.05f; f += Time.deltaTime)
        {
            elementToFade.color = new Color(elementToFade.color.r, elementToFade.color.g, elementToFade.color.b, (1 - fadeType) + (f * reciprocal));
            yield return null;
        }
        elementToFade.color = new Color(elementToFade.color.r, elementToFade.color.g, elementToFade.color.b, fadeType);
    }
    IEnumerator FadeSequence(CanvasGroup elementToFade, float fadeTime, float delayTime, int fadeType)
    {
        elementToFade.alpha = 1 - fadeType;
        yield return new WaitForSeconds(delayTime);
        float reciprocal = 1 / fadeTime;
        if (fadeType == 1)
        {
            reciprocal *= 1;
        }
        else if (fadeType == 0)
        {
            reciprocal *= -1;
        }
        for (float f = 0f; f <= fadeTime * 1.05f; f += Time.deltaTime)
        {
            elementToFade.alpha = (1 - fadeType) + (f * reciprocal);
            yield return null;
        }
        elementToFade.alpha = fadeType;
    }
    int CountDigits(ulong score)
    {
        return (int)Mathf.Floor(Mathf.Log10(score) + 1);
    }
}
