﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleCardFadeIn : MonoBehaviour
{
    private void OnEnable()
    {
        StartCoroutine(FadeSequence(GetComponent<CanvasGroup>(), 0.5f, 0, 1));
    }
    IEnumerator FadeSequence(CanvasGroup elementToFade, float fadeTime, float delayTime, int fadeType)
    {
        elementToFade.alpha = 1 - fadeType;
        yield return new WaitForSeconds(delayTime);
        float reciprocal = 1 / fadeTime;
        if (fadeType == 1)
        {
            reciprocal *= 1;
        }
        else if (fadeType == 0)
        {
            reciprocal *= -1;
        }
        for (float f = 0f; f <= fadeTime * 1.05f; f += Time.deltaTime)
        {
            elementToFade.alpha = (1 - fadeType) + (f * reciprocal);
            yield return null;
        }
        elementToFade.alpha = fadeType;
    }
}
