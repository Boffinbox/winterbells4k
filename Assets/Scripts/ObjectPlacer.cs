﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPlacer : MonoBehaviour
{
    int incrementalBellCount = 0;
    float scaleModifier;
    float bellSeparation = 1.65f;
    [SerializeField] SpriteRenderer background;
    void Start()
    {
    }
    void Update()
    {
        if (Scoring.BellCount < 250)
        {
            scaleModifier = 0.2f - (0.0004f * Scoring.BellCount);
        }
        else
        {
            scaleModifier = 0.1f;
        }
        if (CountActiveBells() == 0)
        {
            SpawnBell();
        }
        else
        {
            if (CountActiveBells() < 16)
            {
                SpawnNextBell();
            }
        }
    }
    GameObject SpawnBell()
    {
        GameObject a = null;
        if (incrementalBellCount <= Random.Range(25, 41))
        {
            a = ObjectPooler.Instance.GetObjectFromPool(ObjectPooler.Instance.PoolOfBells);
            a.GetComponentInChildren<SpriteRenderer>().transform.localScale = new Vector3(scaleModifier, scaleModifier, 1);
        }
        else
        {
            a = ObjectPooler.Instance.GetObjectFromPool(ObjectPooler.Instance.PoolOfBoostBells);
            incrementalBellCount = 0;
        }
        a.transform.SetParent(transform);
        a.transform.position = new Vector2(RandomScreenX(), background.transform.position.y);
        incrementalBellCount++;
        return a;
    }
    GameObject SpawnNextBell()
    {
        GameObject a = SpawnBell();
        a.transform.position = new Vector3(a.transform.position.x, (GetHighestBell().transform.position + (Vector3.up * bellSeparation)).y, 0);
        return a;
    }
    float RandomScreenX()
    {
        return Camera.main.ViewportToWorldPoint(new Vector2(Random.Range(0.05f, 0.95f), 0)).x;
    }
    int CountActiveBells()
    {
        int bellCount = 0;
        foreach (Transform child in transform)
        {
            bellCount++;
        }
        return bellCount;
    }
    GameObject GetHighestBell()
    {
        GameObject a = null;
        foreach (Transform child in transform)
        {
            if (a == null)
            {
                a = child.gameObject;
            }
            else if (a.transform.position.y < child.position.y)
            {
                a = child.gameObject;
            }
        }
        return a;
    }

}
