﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarBurstRotator : MonoBehaviour
{
    void Update()
    {
        transform.localRotation = Quaternion.Euler(0, 0, transform.localEulerAngles.z + (180f * Time.deltaTime));
    }
}
