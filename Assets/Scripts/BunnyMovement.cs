﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BunnyMovement : MonoBehaviour
{
    float speed = 6f;
    float precision = 0.1f;
    static bool hasStarted = false;
    public static bool HasStarted { get => hasStarted; }
    Rigidbody2D rb;
    Animator anim;
    enum Direction { None, Left, Right };
    // quick frame rate checker
    //void OnGUI()
    //{
    //    GUI.Label(new Rect(0, 0, 100, 100), ((int)(1.0f / Time.smoothDeltaTime)).ToString());
    //}
    private void Start()
    {
        //#if UNITY_EDITOR
        //        QualitySettings.vSyncCount = 0;  // VSync must be disabled
        //        Application.targetFrameRate = 150;
        //#endif
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!HasStarted)
            {
                hasStarted = true;
                if (Scoring.BellCount <= 0)
                {
                    GetComponent<Rigidbody2D>().velocity += Vector2.up * 12f; // rescaled scene and gravity to make this number sensible
                }
                else
                {
                    hasStarted = false;
                }
            }
        }
        if (GetMouseDirection() == Direction.Left)
        {
            transform.localScale = new Vector3(-0.2f, 0.2f, 1);
            anim.SetTrigger("running");
        }
        else if (GetMouseDirection() == Direction.Right)
        {
            transform.localScale = new Vector3(0.2f, 0.2f, 1);
            anim.SetTrigger("running");
        }
        else
        {
            anim.SetTrigger("idle");
        }
        if (HasStarted)
        {
            AirMovement();
            anim.SetTrigger("firstJump");
        }
        else
        {
            GroundMovement();
            anim.SetTrigger("landed");
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        hasStarted = false;
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        rb.velocity = Vector2.zero;
        rb.velocity += Vector2.up * 10f;
        anim.SetTrigger("touchedBell");
    }
    void AirMovement()
    {
        float xDistance = Mathf.Abs(MousePosition().x - transform.position.x);
        float speedToBlend = 5.8f;
        if (!PredictedPositionIsOutOfBounds(xDistance * speedToBlend * Time.deltaTime * DirectionSignSetter(GetMouseDirection())))
        {
            transform.position += Vector3.right * (xDistance * speedToBlend * Time.deltaTime ) * DirectionSignSetter(GetMouseDirection());
        }
    }
    void GroundMovement()
    {
        if (!PredictedPositionIsOutOfBounds(speed * Time.deltaTime * DirectionSignSetter(GetMouseDirection())))
        {
            if (MousePosition().x >= transform.position.x + precision || MousePosition().x <= transform.position.x - precision)
            {
                transform.position += Vector3.right * (speed * Time.deltaTime * DirectionSignSetter(GetMouseDirection()));
            }
        }
    }
    bool PredictedPositionIsOutOfBounds(float distanceToTransform)
    {
        Vector2 predictedPosition = Camera.main.WorldToViewportPoint(new Vector3(transform.position.x + distanceToTransform, 0, 0));
        if (predictedPosition.x > 0.95f || predictedPosition.x < 0.05f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    int DirectionSignSetter(Direction dir)
    {
        int sign = 0;
        if (dir == Direction.Left)
        {
            sign = -1;
        }
        else if (dir == Direction.Right)
        {
            sign = +1;
        }
        else if (dir == Direction.None)
        {
            sign = 0;
        }
        return sign;
    }
    Direction GetMouseDirection()
    {
        if (MousePosition().x >= transform.position.x - precision && MousePosition().x <= transform.position.x + precision)
        {
            return Direction.None;
        }
        else if (MousePosition().x < transform.position.x)
        {
            return Direction.Left;
        }
        else if (MousePosition().x > transform.position.x)
        {
            return Direction.Right;
        }
        else
        {
            return Direction.None;
        }
    }
    Vector2 MousePosition()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }
}
