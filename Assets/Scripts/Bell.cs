﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Bell : MonoBehaviour
{
    SpriteRenderer bellSpriteRenderer;
    Canvas scoreCanvas;
    AudioSource playerAudioSource;
    [SerializeField] AudioClip bellSound;
    float initialYRendererOffset = -0.3f;
    float initialScoreOffset = +0.25f;
    ParticleSystem starBurst;
    bool hasReachedBottom;
    private void OnEnable()
    {
        bellSpriteRenderer = GetComponentInChildren<SpriteRenderer>();
        scoreCanvas = GetComponentInChildren<Canvas>();
        bellSpriteRenderer.transform.localPosition = new Vector3(0, initialYRendererOffset, 0);
        scoreCanvas.transform.localPosition = new Vector3(0, initialScoreOffset, 0);
        GetComponent<Collider2D>().enabled = true;
        StartCoroutine(BellRotator());
        hasReachedBottom = false;
    }
    private void Start()
    {
        playerAudioSource = GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>();
        starBurst = GetComponent<ParticleSystem>();
    }
    void Update()
    {
        transform.localPosition += Vector3.down * Time.deltaTime;
        Vector3 screenPos = Camera.main.WorldToViewportPoint(transform.position);
        if (screenPos.y < -0.3f || screenPos.y > 6f)
        {
            ObjectPooler.Instance.ReturnObjectToBellsPool(gameObject);
        }
        if (screenPos.x > 0.95f || screenPos.x < 0.05f)
        {
            transform.position = new Vector3(transform.position.x / (Random.Range(2f, 3f)), transform.position.y, transform.position.z);
        }
        if (transform.position.y <= 3.5f && !hasReachedBottom)
        {
            hasReachedBottom = true;
            StopCoroutine(BellRotator());
            transform.localRotation = Quaternion.Euler(Vector3.zero);
            StartCoroutine(BellFadeSequence(bellSpriteRenderer));
        }
        float currentScale = bellSpriteRenderer.transform.localScale.x;
        GetComponent<BoxCollider2D>().size = new Vector2(currentScale * 2.5f, currentScale * 3f);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        playerAudioSource.PlayOneShot(bellSound);
        StartCoroutine(BellFadeSequence(bellSpriteRenderer));
        StartCoroutine(BellMoveSequence(bellSpriteRenderer));
        StartCoroutine(ScoreFadeSequence());
        StopCoroutine(BellRotator());
        transform.localRotation = Quaternion.Euler(Vector3.zero);
        GetComponent<Collider2D>().enabled = false;
        starBurst.Play();
    }
    IEnumerator BellRotator()
    {
        int direction = 1;
        float rotationSpeed = 9.3f;
        while (true)
        {
            Vector3 rot = bellSpriteRenderer.transform.localEulerAngles;
            if (rot.z >= 5 && rot.z <= 170)
            {
                direction = -1;
            }
            if (rot.z <= 355 && rot.z >= 190)
            {
                direction = 1;
            }
            rot = new Vector3(0, 0, rot.z + (direction * rotationSpeed * Time.deltaTime));
            bellSpriteRenderer.transform.localRotation = Quaternion.Euler(rot);
            yield return null;
        }
    }
    private IEnumerator BellFadeSequence(SpriteRenderer bellRenderer)
    {
        yield return new WaitForSeconds(0.2f);
        while (bellRenderer.color.a >= 0)
        {
            Color c = bellRenderer.color;
            c.a -= Time.deltaTime * 5f;
            bellRenderer.color = c;
            yield return null;
        }
        bellRenderer.color = new Color(1, 1, 1, 0);
        while (!starBurst.isStopped)
        {
            yield return null;
        }
        transform.localRotation = Quaternion.Euler(Vector3.zero);
        ObjectPooler.Instance.ReturnObjectToBellsPool(gameObject);
        yield return null;
    }
    private IEnumerator BellMoveSequence(SpriteRenderer bellRenderer)
    {
        // this method shamelessly stolen from the unity manual!
        for (float fadeTimer = 1f; fadeTimer >= -0.01f; fadeTimer -= (Time.deltaTime * 2.5f))
        {
            bellRenderer.transform.localPosition = new Vector2(0, 4.4f * ((fadeTimer - 0.5f) * (fadeTimer - 0.5f)) + ( -(1 - initialYRendererOffset))); // quadratic to animate the bell
            yield return null;
        }
    }
    IEnumerator ScoreFadeSequence()
    {
        scoreCanvas.GetComponentInChildren<TextMeshProUGUI>().color = new Color(1, 1, 1, 1);
        yield return new WaitForSeconds(0.4f);
        for (float fadeTimer = 0.1f; fadeTimer >= -0.01f; fadeTimer -= (Time.deltaTime))
        {
            scoreCanvas.GetComponentInChildren<TextMeshProUGUI>().color = new Color(1, 1, 1, fadeTimer * 10);
            yield return null;
        }
        scoreCanvas.GetComponentInChildren<TextMeshProUGUI>().color = new Color(1, 1, 1, 0);
        yield return null;
    }
}
