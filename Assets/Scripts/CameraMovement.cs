﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    GameObject player;
    Vector3 initialPosition;
    [SerializeField] SpriteRenderer background;
    float minScreenRatio = 0.8f;
    float maxScreenRatio = 2f;
    float currentScreenRatio;
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        transform.position = new Vector3(0, Camera.main.orthographicSize, -10);
        SetOrthographicSize();
        initialPosition = transform.position;
    }
    private void Update()
    {
        float x = Screen.width;
        float y = Screen.height;
        float screenRatio = x / y;
        if (currentScreenRatio != screenRatio)
        {
            SetOrthographicSize();
        }
        float yDistance = player.transform.position.y - transform.position.y;
        float speedToBlend = 5f;
        if (player.transform.position.y >= Camera.main.orthographicSize)
        {
            if (player.GetComponent<Rigidbody2D>().velocity.y <= -20f)
            {
                transform.position = new Vector3(initialPosition.x, player.transform.position.y, initialPosition.z);
            }
            else
            {
                transform.position = new Vector3(initialPosition.x, transform.position.y + (yDistance * speedToBlend * Time.deltaTime), initialPosition.z);
            }
        }
        else
        {
            transform.position = new Vector3(0, Camera.main.orthographicSize, -10);
        }
    }
    void SetOrthographicSize()
    {
        float x = Screen.width;
        float y = Screen.height;
        float screenRatio = x / y;
        if (screenRatio >= minScreenRatio && screenRatio <= maxScreenRatio)
        {
            Camera.main.orthographicSize = (background.bounds.size.x * (1 / screenRatio)) * 0.5f;
        }
        else if (screenRatio > maxScreenRatio)
        {
            Camera.main.orthographicSize = (background.bounds.size.x * (1 / maxScreenRatio)) * 0.5f;
        }
        else if (screenRatio < minScreenRatio)
        {
            Camera.main.orthographicSize = (background.bounds.size.x * (1 / minScreenRatio)) * 0.5f;
        }
        currentScreenRatio = screenRatio;
    }
}