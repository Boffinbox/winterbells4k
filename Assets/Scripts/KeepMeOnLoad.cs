﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepMeOnLoad : MonoBehaviour
{
    static KeepMeOnLoad Instance;
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
