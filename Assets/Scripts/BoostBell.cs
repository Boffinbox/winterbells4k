﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BoostBell : MonoBehaviour
{
    Vector3 directionVector;
    AudioSource playerAudioSource;
    [SerializeField] AudioClip boostSound;
    float initialYRendererOffset = -0.4f;
    ParticleSystem starBurst;
    SpriteRenderer boostSpriteRenderer;
    bool hasReachedBottom;
    enum MoveMode { Normal, Collected };
    MoveMode moveMode;
    private void OnEnable()
    {
        boostSpriteRenderer = GetComponentInChildren<SpriteRenderer>();
        GetComponentInChildren<SpriteRenderer>().transform.localPosition = new Vector3(0, initialYRendererOffset, 0);
        moveMode = MoveMode.Normal;
        int coinFlip = Random.Range(0, 2);
        if (coinFlip == 0)
        {
            directionVector = Vector3.left;
            boostSpriteRenderer.transform.localScale = new Vector3(-0.2f, 0.2f, 1);
        }
        else
        {
            directionVector = Vector3.right;
            boostSpriteRenderer.transform.localScale = new Vector3(0.2f, 0.2f, 1);
        }
    }
    private void Start()
    {
        boostSpriteRenderer = GetComponentInChildren<SpriteRenderer>();
        playerAudioSource = GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>();
        starBurst = GetComponentInChildren<ParticleSystem>();
    }
    void Update()
    {
        Vector3 screenPos = Camera.main.WorldToViewportPoint(transform.position);
        transform.localPosition += Vector3.down * Time.deltaTime;
        if (moveMode == MoveMode.Normal)
        {
            transform.localPosition += directionVector * 2 * Time.deltaTime;
            if (screenPos.x > 0.95f)
            {
                directionVector = Vector3.left;
                boostSpriteRenderer.transform.localScale = new Vector3(-0.2f, 0.2f, 1);
                if (screenPos.x > 1.05f)
                {
                    transform.position = new Vector3(transform.position.x / (Random.Range(2f, 3f)), transform.position.y, transform.position.z);
                }
            }
            else if (screenPos.x < 0.05f)
            {
                directionVector = Vector3.right;
                boostSpriteRenderer.transform.localScale = new Vector3(0.2f, 0.2f, 1);
                if (screenPos.x < -0.05f)
                {
                    transform.position = new Vector3(transform.position.x / (Random.Range(2f, 3f)), transform.position.y, transform.position.z);
                }
            }
            if (screenPos.y < -0.3f || screenPos.y > 6f)
            {
                ObjectPooler.Instance.ReturnObjectToBoostBellsPool(gameObject);
            }
            if (transform.position.y <= 3.5f && !hasReachedBottom)
            {
                hasReachedBottom = true;
                StartCoroutine(BellFadeSequence(boostSpriteRenderer));
            }
        }
        else if (moveMode == MoveMode.Collected)
        {
            transform.localPosition += directionVector * 15 * Time.deltaTime;
            if (screenPos.x > 1.5f || screenPos.x < -0.5f)
            {
                moveMode = MoveMode.Normal;
                StopCoroutine(BoostBellMoveSequence());
                ObjectPooler.Instance.ReturnObjectToBoostBellsPool(gameObject);
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        StartCoroutine(BoostBellMoveSequence());
        playerAudioSource.PlayOneShot(boostSound);
        starBurst.Play();
    }
    IEnumerator BoostBellMoveSequence()
    {
        for (float countDown = 1; countDown >= -0.01f; countDown -= Time.deltaTime * 20f)
        {
            transform.localPosition += Vector3.down * 12 * Time.deltaTime;
            yield return null;
        }
        yield return new WaitForSeconds(0.25f);
        moveMode = MoveMode.Collected;
        yield return null;
    }
    private IEnumerator BellFadeSequence(SpriteRenderer bellRenderer)
    {
        yield return new WaitForSeconds(0.2f);
        while (bellRenderer.color.a >= 0)
        {
            Color c = bellRenderer.color;
            c.a -= Time.deltaTime * 5f;
            bellRenderer.color = c;
            yield return null;
        }

        ObjectPooler.Instance.ReturnObjectToBoostBellsPool(gameObject);
        bellRenderer.color = new Color(1, 1, 1, 0);
        yield return null;
    }
}
